#all:
#	g++ -g main.cpp input/input.cpp matrix/matrix.cpp matlab/matlab.cpp rotations/rotations.cpp forward/forward.cpp reverse/reverse.cpp utilities/utilities.cpp -o prog1
#clean:
#	rm -r prog1*

# http://mrbook.org/tutorials/make/
CC=g++
CFLAGS=-c -Wall
SOURCES=src/main.cpp src/matrix/matrix.cpp src/matlab/matlab.cpp src/reverse/reverse.cpp src/forward/forward.cpp src/utilities/utilities.cpp src/input/input.cpp src/rotations/rotations.cpp
OBJECTS=$(SOURCES:.cpp=.o)
EXECUTABLE=prog1

all: $(SOURCES) $(EXECUTABLE)
$(EXECUTABLE): $(OBJECTS) 
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@
.cpp.o:
	$(CC) $(CFLAGS) $< -o $@
clean:
	find . -name '*.o' -print0 | xargs -0 rm -v
	rm -v prog1
