EECS390-Prog1
=============

Robotics Project #1

EECS-390 Spring/2013
Instructor: Prof. Chi-haur Wu                                    
PROGRAM PROJECT #1

Write a C program to plan the orientation of a robot using three basic methods: 
(1) one-axis rotation, (2) EULER angles, and (3) RPY angles. 

The structure of your program MUST have the following subroutines:
(1) One axis rotation transformation and its inverse solutions. 
(2) EULER transformation and its inverse solutions.
(3) RPY transformation and its inverse solutions.

Use the following three transformations to test your program. You should use the read_in subroutine to read the following input data.

POS1 =  
0	-1	0	260
0	0	1	300
-1	0	0	480
0	0	0	1

POS2 = 
1	0	0	100
0	1	0	200
0	0	1	280
0	0	0	1

POS3 =  
0	0	-1	-400
1	0	0	140
0	-1	0	120
0	0	0	1

Your program must do: 

(1) Read a input file named input_data13.txt. The input data should be the given three transformations. After you read the input, compute the inverse solutions of all three transforms based on the given three types of rotational planning.

(2) Rotational path planning: (ignore the position vectors in POS1, POS2, POS3.)
Your planning of orientations will have 7 iterations. Initially (iteration=0), robot hand's orientation is represented by POS1. At iteration = 3, robot hand's orientation will be represented by POS2. Then at iteration = 6, robot hand's orientation will be represented by POS3. For each orientational planning, at each iteration you need to compute the values of your planning variables (RPY, EULER or one-axis) and their corresponding forward rotational transformation (n-vector, o-vector, a-vector). Generate Excel plots of planning variables for each planning.

(3) Translational path planning:
For every iteration of your rotational planning, the position (p-vector) will be planned by linear interpolation. Generate Excel plots of your planning.

(4) Robot position and orientation planning for 7 iterations, i=0 to 6:

POS(i) =  
n(i)	o(i)	a(i)	p(i)
0		0		0		1

POS(i) is the orientational planning in (2) and translational planning in (3). you need to output 7 POS transformations for robot to move to. Draw these 7 planned transformations in a 3D graph.

Note:  20% of the total credit will be the comments in your program.


EECS390-Prog2
=============

The Cartesian position of a robot manipulator can be represented by the following transformation equation:     
POS = BASE * T6 * TOOL
where T6 = A1*A2*A3*A*A5*A6. 

Write a program that solves joint positions and computes T6 matrix for PUMA 560 robots. Use the following three Cartesian positions to test your program.

        1  0   0  260           0   0 -1  250            0  1  0 430   
 POS1 = 0 -1   0  390    POS2 = 1   0  0  340   POS3 =   0  0  1 400    
        0  0  -1  580           0  -1  0  220            1  0  0 380  
        0  0   0   1   ,        0   0  0   1  ,          0  0  0  1   .

The geometrical kinematic data of PUMA arm are defined as follows:
     d1 = 500.0 mm, a2 = 431.8 mm, d3 = 149.09 mm, a3 = -20.32 mm,
     d4 = 433.07 mm, and d6 = 56.25 mm.

The mechanical limits of the joint variables are:
(1) TH1 = [ -160 deg, +160 deg],      (2) TH2 = [ -223 deg, +43 deg],
(3) TH3 = [ -52 deg, +232 deg],       (4) TH4 = [ -110 deg, +170 deg],
(5) TH5 = [ -100 deg, +100 deg],      (6) TH6 = [ -266 deg, +266 deg].

a)	Put d1 in BASE and d6 in TOOL. Joint solutions can then be solved from T6.
b)	Since a PUMA robot has choices of righty, lefty, above, below, flip and nonflip, there are eight possible configurations to solve joint angles from any given Cartesian position. 
c)	Your program also must check joint limits so that the solved joint angles are always within the mechanical limits for any given Cartesian Position and orientation. If any joint is out of limit, flag the output. 

Your program will do the following: 
(1) Your program will test all eight configurations in the exact sequence as follows: (R,A,NF), (R,A,F), (R,B,NF), (R,B,F), (L,A,NF), (L,A,F), (L,B,NF) and (L,B,F).

(2) Test inverse and forward kinematics:
For each configuration, you use the three input transformations to solve the corresponding joint angles, and then you use the forward transformation to compute the original input transformations to verify your solutions. Output the solved joint positions and the computed forward transformation for the assigned configuration. (Note: Only two configurations work!)

(3) Simple joint path planning for the valid configurations:
    Your planning of robot's path will have five iterations. Initially (the first iteration), the position and orientation of this robot's hand is at POS1. At the third iteration, the position and orientation of robot's hand will be at POS2. At the fifth iterations, the robot hand's position will be at POS3. As for the two intermediate Cartesian positions, they will be planned and computed based on the intermediate joint angles between the solved three joint positions at the three given Cartesian positions. For each robot configuration, output the planned five Cartesian positions and their corresponding six joint angles and six EULER positions (3 EULAR angles and (px, py, pz)).

4)	Cartesian EULER path planning:
Based on your program#1, robot’s Cartesian path (transformation) can be planned by 3 EULER angles and (px, py, pz). Now using the same planning method with 5 iterations for the given 3 positions, compute PUMA joint positions at these 5 planned Cartesian positions under these two configuration: (R,A,F) and (L,A,NF).


