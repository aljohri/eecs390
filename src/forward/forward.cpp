#include <iostream>
#include <cmath>

#include "../matrix/matrix.h"
#include "../exception/exception.h"
#include "../forward/forward.h"
#include "../rotations/rotations.h"

using namespace std;

#define PI 3.1415926535897932384626433832795

void forwardTransform(int iter, Matrix &output1, Matrix &output2, Matrix &output3, double *angleaxis, double *euler, double *rpy) {
    //A function which runs each of the 3 other functions in this file during each iteration of the program, and stores the results.
	//We will call each of the 3 forward transforms each iteration, and the arguments angleaxis, euler, and rpy point to the start of an
	//array storing the 3-4 arguments other than output each of those functions needs, while each of the output matrices is one of the 3
	//transformation matrices calculated by the function.

	//As it takes 3 iterations to go from one of our input position matrices to another, this function does slighly different things each 
	//iteration mod 3. For our 1-axis rotations, we just rotate 1/3 of the degree; however, for the euler/rpy rotations we will rotate one
	//of the three angles at a time, in the proper order.

    cout << "% Values used for Forward Rotation Matrices of Iteration " << iter << endl << endl; //Print the iteration number
    
	if ((iter % 3)== 0) {//if we're the first iteration after one of the given matrices
        cout  << "it" << iter << "_trans_theta_ang = " << angleaxis[0]/3 << ";" << endl;
        cout  << "it" << iter << "_trans_axis_ang = [" << angleaxis[1] << ", " << angleaxis[2] << ", " <<  angleaxis[3] << "];" << endl;
        cout  << "it" << iter << "_trans_values_eul = [" << 0 << ", " << 0 << ", " <<  euler[2] << "];" << endl;
        cout  << "it" << iter << "_trans_values_ang = [" << rpy[0] << ", " << 0 << ", " <<  0 << "];" << endl;
		//Print out the inputs, so we can easily tell if everything's all right.

		oneaxisforward(output1, angleaxis[0]/3, angleaxis[1], angleaxis[2], angleaxis[3]);//then run 1-axis
		eulerforward(output2, euler[0]/3, euler[1]/3, euler[2]/3); //euler
		rpyforward(output3, rpy[0], 0, 0);// and run rpy;
 	}
 	else if ((iter % 3) == 1) {//in the second iteration, same as iteration 1 except rpy/euler use a different one of their input angles.
        cout  << "it" << iter << "_trans_theta_ang = " << angleaxis[0]/3 << ";" << endl;
        cout  << "it" << iter << "_trans_axis_ang = [" << angleaxis[1] << ", " << angleaxis[2] << ", " <<  angleaxis[3] << "];" << endl;
        cout  << "it" << iter << "_trans_values_eul = [" << 0 << ", " << euler[1] << ", " <<  0 << "];" << endl;//different angle use from 1
        cout  << "it" << iter << "_trans_values_ang = [" << 0 << ", " << rpy[1] << ", " <<  0 << "];" << endl;//different
		//print inputs

		oneaxisforward(output1, angleaxis[0]/3, angleaxis[1], angleaxis[2], angleaxis[3]); // then run
		eulerforward(output2, euler[0]/3, euler[1]/3, euler[2]/3); //euler
		rpyforward(output3, 0, rpy[1], 0);
 	}
 	else if ((iter % 3) == 2) {//again, we rotate 1 of the 3 angles for rpy/euler
        cout  << "it" << iter << "_trans_theta_ang = " << angleaxis[0]/3 << ";" << endl;
        cout  << "it" << iter << "_trans_axis_ang = [" << angleaxis[1] << ", " << angleaxis[2] << ", " <<  angleaxis[3] << "];" << endl;
        cout  << "it" << iter << "_trans_values_eul = [" << euler[0] << ", " << 0 << ", " <<  0 << "];" << endl;
        cout  << "it" << iter << "_trans_values_ang = [" << 0 << ", " << 0 << ", " <<  rpy[2] << "];" << endl;
		//print inputs

		oneaxisforward(output1, angleaxis[0]/3, angleaxis[1], angleaxis[2], angleaxis[3]);// then run transforms
		eulerforward(output2, euler[0]/3, euler[1]/3, euler[2]/3); //euler
		rpyforward(output3, 0, 0, rpy[2]);
    }
	else { throw Exception("Iteration number exceeded");} //should be impossible as we work mod 3, but might as well play it safe.

    cout << endl;
}

void oneaxisforward(Matrix &output, double theta, double x, double y, double z) {
	//Function for one-axis forward rotation. 
	double s = sin(theta); double c = cos(theta); double v = 1-c; //define versine, and create an easy shorthand for cos/sine/versine
	//So, we calculate sin/cos/versine

	//Then we set the values of the output matrix, using
	//Nicely formatted code of the functions in the order they appear in the matrix. All functions are as taken from the in-class handout.
	output(1,1)= x*x*v+c;    output(1,2)= y*x*v-z*s;  output(1,3)= z*x*v+y*s;  output(1,4)= 0;
	output(2,1)= x*y*v+z*s;  output(2,2)= y*y*v+c;    output(2,3)= z*y*v-x*s;  output(2,4)= 0;
	output(3,1)= x*z*v-y*s;  output(3,2)= y*z*v+x*s;  output(3,3)= z*z*v+c;    output(3,4)= 0;
	output(4,1)= 0;          output(4,2)= 0;          output(4,3)= 0;          output(4,4)= 1;
}


void eulerforward(Matrix &output, double phi, double theta, double psi) {
	//Forward transform for euler rotation. Since euler rotation is just 1-axis rotation around z, then y, then z again,
	//we just call xyz-axis rotation on each of the 3 angles in order. For fixed-axis rotation functions, see rotations.h.
	output = rotz(phi) * roty(theta) * rotz(psi);//Rotate around z, then y, then z
}

void rpyforward(Matrix &A, double r, double p, double y) {
	//Forward transform for rpy rotation. As in 1-axis we will use a prexisting set of equations.
    double cr = cos(r); double sr = sin(r);
    double cy = cos(y); double sy = sin(y);
    double cp = cos(p); double sp = sin(p);   
	//First, we create an easy shorthand for cos/sine of our 3 angles
	
	//Then we set the values of the output matrix, using code of the functions formattef in the order they appear in the matrix,
	//except each column (in this code) is actually a row (to fix).
	A(1,1)= cy*cp;            A(2,1)= sy * cp;         A(3,1)= -1 * sp;  A(4,1)= 0;
	A(1,2)= cy*sp*sr-sy*cr;   A(2,2)= sy*sp*sr+cy*cr;  A(3,2)= cp*sr;    A(4,2)= 0;
	A(1,3)= cy* sp*cr+sy*sr;  A(2,3)= sy*sp*cr-cy*sr;  A(3,3)= cp*cr;    A(4,3)= 0;
	A(1,4)= 0;                A(2,4)= 0;               A(3,4)= 0;        A(4,4)= 1;
}
