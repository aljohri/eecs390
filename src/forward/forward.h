#ifndef FORWARD_H
#define FORWARD_H
//Protype declarations for our forward transformation functions.
#include "../matrix/matrix.h"

void oneaxisforward(Matrix &output, double theta, double kx, double ky, double kz);//one-axis rotation forward transform
void eulerforward(Matrix &output, double a, double b, double y);//euler rotation forward transform
void rpyforward(Matrix &output, double r, double p, double y);//rpy rotation forward transform

void forwardTransform(int iteration, Matrix &output1, Matrix &output2, Matrix &output3, double *angleaxis, double *euler, double *rpy);
//A function which runs each of the above 3 functions during each iteration of the program, and stores the results.

#endif
