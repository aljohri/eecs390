#include <cstdlib>
#include <cstdio>
#include <vector>
#include <iostream>
#include <fstream>

#include "../input/input.h"
#include "../matrix/matrix.h"

#define MATRIXSIZE 16
#define NUMMATRICIES 3

using namespace std;

std::vector<int> getInput() {

	ifstream myfile ("input.txt");
	// if (!myfile.is_open()) { exit("file could not be opened"); }
	
	std::vector<int> input;
	
	char number[256];
	int inputCounter = 1;
	while(myfile.getline(number, 256, '\t'))
	{
		char * number2;
		if (number2 = strchr(number, '\n')) {
			*(number2) = '\0';
			number2++;
			
			int num = atoi(number); input.push_back(num);
			if ((inputCounter) % 16 == 0) {} // new matrix
			inputCounter++;
			// prevent last newline character from messing things up
			if (inputCounter == ((MATRIXSIZE * NUMMATRICIES) + 1)) {
				break;
			}
			int num2 = atoi(number2); input.push_back(num2); 
			inputCounter++;
		}
		else {
			int num = atoi(number);
			// cout << num << " ";
			input.push_back(num);
			inputCounter++;			
		}
	}	

  return input;

}

// construct matricies by parsing input vector
void parseInput(vector<int> input, Matrix ** matricies) {
	int counter, matrix_counter, row, col;
	counter = matrix_counter = 0;
	for(vector<int>::const_iterator i = input.begin(); i != input.end(); ++i) {
		if (counter % 16 == 0) { matrix_counter++; col = row = 0; }
		else if (counter % 4 == 0) { row++; col = 0; }
		matricies[matrix_counter - 1]->set(row+1,col+1,*i);
		col++;
		counter++;
	}	
}