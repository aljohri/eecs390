#ifndef INPUT_H
#define INPUT_H

#include <vector>
#include "../matrix/matrix.h"

std::vector<int> getInput();
void parseInput(std::vector<int> input, Matrix ** matricies);

#endif