#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <iostream>
#include <vector>

#include "matrix/matrix.h"
#include "input/input.h"
#include "forward/forward.h"
#include "reverse/reverse.h"
#include "exception/exception.h"
#include "matlab/matlab.h"
#include "utilities/utilities.h"

#define PI 3.1415926535897932384626433832795

using namespace std;

void setPosition(Matrix A, int P1, int P2, int P3, int P4);
void setPosition(Matrix &A, Matrix &B);
void forwardTransform(int iteration, Matrix &output1, Matrix &output2, Matrix &output3, double *angleaxis, double *euler, double *rpy);

Matrix * iterations_ang[7] = { new Matrix(4,4), new Matrix(4,4), new Matrix(4,4), new Matrix(4,4), new Matrix(4,4), new Matrix(4,4), new Matrix(4,4) };
Matrix * iterations_eul[7] = { new Matrix(4,4), new Matrix(4,4), new Matrix(4,4), new Matrix(4,4), new Matrix(4,4), new Matrix(4,4), new Matrix(4,4) };
Matrix * iterations_rpy[7] = { new Matrix(4,4), new Matrix(4,4), new Matrix(4,4), new Matrix(4,4), new Matrix(4,4), new Matrix(4,4), new Matrix(4,4) };

int main() {
	cout << "clear; clc;" << endl;

	cout << "% Best Usage: ./prog1 > output.m" << endl;
	cout << "% Output of c++ program is MATLAB friendly for graphing and analyzing." << endl;

	cout << endl;

	cout << "% ----------------------------------------------------------------------------------------" << endl;
	cout << "% Pre-Iterations: Setting up Data" << endl;
	cout << "% ----------------------------------------------------------------------------------------" << endl << endl;

	cout << "% Reading Input ..." << endl << endl;

	std::vector<int> input = getInput();
	Matrix * matricies[3] = { new Matrix(4,4), new Matrix(4,4), new Matrix(4,4) };
	parseInput(input, matricies);	

	cout << "POS1 = ..." << endl; matricies[0]->Print(); cout << endl;
	cout << "POS2 = ..." << endl; matricies[1]->Print(); cout << endl;
	cout << "POS3 = ..." << endl; matricies[2]->Print(); cout << endl;

	cout << "% Calculating Transformation Matricies ..." << endl << endl;
	
	Matrix * transforms[2] = { new Matrix(4,4), new Matrix(4,4) };
	cout << "TRANS12 = ..." << endl; transforms[0] = new Matrix(*(matricies[1]) * InvTrans(*(matricies[0]))); transforms[0]->Print(); cout << endl;
	cout << "TRANS23 = ..." << endl; transforms[1] = new Matrix(*(matricies[2]) * InvTrans(*(matricies[1]))); transforms[1]->Print(); cout << endl;

	Matrix currentPositionAngleVec = Matrix(4,4), currentPositionEuler = Matrix(4,4), currentPositionRPY = Matrix(4,4);

	currentPositionAngleVec = *(matricies[0]);
	currentPositionEuler = *(matricies[0]);
	currentPositionRPY = *(matricies[0]);

	for (int i = 0; i < 3; i++) {

		double angleaxis[4], euler[3], rpy[3];
		Matrix translate = Matrix(4,4);

		for (int j = 0; j < 3; j++) {
			int iteration = 3*i + j;
			if (iteration == 7) break;

			const char * text1 = (j == 0) ? "% Calculate Reverse Rotation Matrices and First Forward Transformation" : "Calculate Forward Transformation";
			
			cout << "% ----------------------------------------------------------------------------------------" << endl;
			cout << "% Iteration: " << iteration << " -- " <<  text1 << endl;
			cout << "% ----------------------------------------------------------------------------------------" << endl << endl;
			
			cout << "% Current Position/Orientation" << endl << endl;
			*(iterations_ang[iteration]) = currentPositionAngleVec; *(iterations_eul[iteration]) = currentPositionEuler; *(iterations_rpy[iteration]) = currentPositionRPY;

			cout << "it" << iteration << "_pos = [" << currentPositionAngleVec(1,4) << "; " << currentPositionAngleVec(2,4) << "; " << currentPositionAngleVec(3,4) << "; " << currentPositionAngleVec(4,4) << "];" << endl << endl;
			cout << "it" << iteration << "_pos_ang = [ ..." << endl; currentPositionAngleVec.Print(); cout << endl;
			cout << "it" << iteration << "_pos_eul = [ ..." << endl; currentPositionEuler.Print(); cout << endl;
			cout << "it" << iteration << "_pos_rpy = [ ..." << endl; currentPositionRPY.Print(); cout << endl;
			cout << endl;

			Matrix output1, output2, output3;
			output1 = Matrix(4,4);
			output2 = Matrix(4,4);
			output3 = Matrix(4,4);

			if (j == 0) {
				// Reverse Rotation Matrix for Position Matrix
				cout << "% Reverse Rotation Matrix for Position " << i+1<< ": Inverse Solutions" << endl << endl;
				reverseTransform(*(matricies[i]), angleaxis, euler, rpy);
				cout << "rev_mat_pos" << i+1 << "_ang = [" << angleaxis[0] * 180/PI  <<  ", " << angleaxis[1] << ", " << angleaxis[2] << ", " << angleaxis[3] << "];" << endl;
				cout << "rev_mat_pos" << i+1 << "_eul = [" << euler[0] * 180/PI << ", " << euler[1] * 180/PI << ", " << euler[2] * 180/PI << "];" << endl;
				cout << "rev_mat_pos" << i+1 << "_rpy = [" << rpy[2] * 180/PI << ", " << rpy[1] * 180/PI << ", " << rpy[0] * 180/PI << "];" << endl;
				cout << endl;

				if (iteration == 6) break;

				// Reverse Rotation Matrix for Transformation Matrix between Position n and Position n+1
				cout << "% Reverse Rotation Matrix for Transformation from Position " << i+1 << " to Position " << i+2 << endl << endl;
				reverseTransform(*(transforms[i]), angleaxis, euler, rpy);
				cout << "rev_mat_trans" << i+1 << i+2 << "_ang = [" << angleaxis[0] * 180/PI << ", " << angleaxis[1] << ", " << angleaxis[2] << ", " << angleaxis[3] << "];" << endl;
				cout << "rev_mat_trans" << i+1 << i+2 << "_eul = [" << euler[0] * 180/PI << ", " << euler[1] * 180/PI << ", " << euler[2] * 180/PI << "];" << endl;
				cout << "rev_mat_trans" << i+1 << i+2 << "_rpy = [" << rpy[2] * 180/PI << ", " << rpy[1] * 180/PI << ", " << rpy[0] * 180/PI << "];" << endl;
				cout << endl;

				Matrix temp = *(matricies[i+1]) - *(matricies[i]);
				setPosition(translate, temp);
				translate(4,4) = 0;
			}

			// Calculate Forward Transformations based on the current iteration (one-axis, euler, and RPY)
			forwardTransform(iteration, output1, output2, output3, angleaxis, euler, rpy);

			cout << "% Forward Rotation Matrices of Iteration " << iteration << " " << endl << endl;
			cout  << "it" << iteration << "_rot_ang= ..." << endl; output1.Print(); cout << endl;
			cout  << "it" << iteration << "_rot_eul= ..." << endl; output2.Print(); cout << endl;
			cout  << "it" << iteration << "_rot_rpy= ..." << endl; output3.Print(); cout << endl;
			cout << endl;

			// Store Current Positions in temporary array before rotations are applied
			double currentPosition[4] = { currentPositionAngleVec(1,4), currentPositionAngleVec(2,4), currentPositionAngleVec(3,4), currentPositionAngleVec(4,4) };

			// Apply Rotation
			currentPositionAngleVec = output1 * currentPositionAngleVec;
			currentPositionEuler = output2 * currentPositionEuler;
			currentPositionRPY = output3 * currentPositionRPY;

			// Current Position Matricies have Position Vector as a Column Vector in the 4th Column
			// These Columns are reset upon multiplying by rotation matricies. Thus we temporarily store them
			// in the array currentPosition[4] and reset them after the rotations are applied
			currentPositionAngleVec(1,4) = currentPosition[0]; currentPositionAngleVec(2,4) = currentPosition[1]; currentPositionAngleVec(3,4) = currentPosition[2]; currentPositionAngleVec(4,4) = currentPosition[3];
			currentPositionEuler(1,4) = currentPosition[0]; currentPositionEuler(2,4) = currentPosition[1]; currentPositionEuler(3,4) = currentPosition[2]; currentPositionEuler(4,4) = currentPosition[3];
			currentPositionRPY(1,4) = currentPosition[0]; currentPositionRPY(2,4) = currentPosition[1]; currentPositionRPY(3,4) = currentPosition[2]; currentPositionRPY(4,4) = currentPosition[3];

			// Apply Translation
			currentPositionAngleVec = currentPositionAngleVec + translate/3;
			currentPositionEuler = currentPositionEuler + translate/3;
			currentPositionRPY = currentPositionRPY + translate/3;

		}
	}

	matlab(iterations_ang, iterations_eul, iterations_rpy);
}