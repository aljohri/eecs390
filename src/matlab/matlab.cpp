#include <iostream>

#include "../matlab/matlab.h"
#include "../matrix/matrix.h"

using namespace std;

void matlab(Matrix ** iterations_ang, Matrix ** iterations_eul, Matrix ** iterations_rpy) {

	cout << "% MATLAB PLOTTING COMMANDS" << endl << endl;
	cout << "% One-Axis Rotation: " << endl << endl;
	matlab_ang(iterations_ang); cout << endl;

	cout << "uiwait;" << endl;

	cout << "% Euler Rotation: " << endl << endl;
	matlab_eul(iterations_eul); cout << endl;

	cout << "uiwait;" << endl;

	cout << "% RPY Rotation: " << endl << endl;
	matlab_rpy(iterations_rpy); cout << endl;
}

char getcolor(int i) {
	if (i == 0) 				return 'g';
	else if (i > 0 &&  i < 3) 	return 'k';
	else if (i == 3) 			return 'b';
	else if (i >3 && i < 6)		return 'm';
	else if (i == 6)			return 'r';
}

void matlab_ang(Matrix ** iterations_ang) {
	cout << "clf;" << endl;
	cout << "hold on;" << endl;
	cout << "grid on;" << endl;

	for (int i = 0; i <= 6; i++) {
		char color = getcolor(i);
		cout << "quiver3(" << iterations_ang[i]->get(1,4) << ", " << iterations_ang[i]->get(2,4) << ", " << iterations_ang[i]->get(3,4) << ", " << iterations_ang[i]->get(1,1) << ", " << iterations_ang[i]->get(2,1) << ", " << iterations_ang[i]->get(3,1) << ", " << "30" << ", '" << color << "');" << endl;
		cout << "quiver3(" << iterations_ang[i]->get(1,4) << ", " << iterations_ang[i]->get(2,4) << ", " << iterations_ang[i]->get(3,4) << ", " << iterations_ang[i]->get(1,2) << ", " << iterations_ang[i]->get(2,2) << ", " << iterations_ang[i]->get(3,2) << ", " << "30" << ", '" << color << "');" << endl;
		cout << "quiver3(" << iterations_ang[i]->get(1,4) << ", " << iterations_ang[i]->get(2,4) << ", " << iterations_ang[i]->get(3,4) << ", " << iterations_ang[i]->get(1,3) << ", " << iterations_ang[i]->get(2,3) << ", " << iterations_ang[i]->get(3,3) << ", " << "30" << ", '" << color << "');" << endl;		
	}

	cout << "hold off;" << endl;
}

void matlab_eul(Matrix ** iterations_eul) {
	cout << "clf;" << endl;
	cout << "hold on;" << endl;
	cout << "grid on;" << endl;

	for (int i = 0; i <= 6; i++) {
		char color = getcolor(i);
		cout << "quiver3(" << iterations_eul[i]->get(1,4) << ", " << iterations_eul[i]->get(2,4) << ", " << iterations_eul[i]->get(3,4) << ", " << iterations_eul[i]->get(1,1) << ", " << iterations_eul[i]->get(2,1) << ", " << iterations_eul[i]->get(3,1) << ", " << "30" << ", '" << color << "');" << endl;
		cout << "quiver3(" << iterations_eul[i]->get(1,4) << ", " << iterations_eul[i]->get(2,4) << ", " << iterations_eul[i]->get(3,4) << ", " << iterations_eul[i]->get(1,2) << ", " << iterations_eul[i]->get(2,2) << ", " << iterations_eul[i]->get(3,2) << ", " << "30" << ", '" << color << "');" << endl;
		cout << "quiver3(" << iterations_eul[i]->get(1,4) << ", " << iterations_eul[i]->get(2,4) << ", " << iterations_eul[i]->get(3,4) << ", " << iterations_eul[i]->get(1,3) << ", " << iterations_eul[i]->get(2,3) << ", " << iterations_eul[i]->get(3,3) << ", " << "30" << ", '" << color << "');" << endl;		
	}

	cout << "hold off;" << endl;
}

void matlab_rpy(Matrix ** iterations_rpy) {
	cout << "clf;" << endl;
	cout << "hold on;" << endl;
	cout << "grid on;" << endl;

	for (int i = 0; i <= 6; i++) {
		char color = getcolor(i);
		cout << "quiver3(" << iterations_rpy[i]->get(1,4) << ", " << iterations_rpy[i]->get(2,4) << ", " << iterations_rpy[i]->get(3,4) << ", " << iterations_rpy[i]->get(1,1) << ", " << iterations_rpy[i]->get(2,1) << ", " << iterations_rpy[i]->get(3,1) << ", " << "30" << ", '" << color << "');" << endl;
		cout << "quiver3(" << iterations_rpy[i]->get(1,4) << ", " << iterations_rpy[i]->get(2,4) << ", " << iterations_rpy[i]->get(3,4) << ", " << iterations_rpy[i]->get(1,2) << ", " << iterations_rpy[i]->get(2,2) << ", " << iterations_rpy[i]->get(3,2) << ", " << "30" << ", '" << color << "');" << endl;
		cout << "quiver3(" << iterations_rpy[i]->get(1,4) << ", " << iterations_rpy[i]->get(2,4) << ", " << iterations_rpy[i]->get(3,4) << ", " << iterations_rpy[i]->get(1,3) << ", " << iterations_rpy[i]->get(2,3) << ", " << iterations_rpy[i]->get(3,3) << ", " << "30" << ", '" << color << "');" << endl;		
	}

	cout << "hold off;" << endl;
}