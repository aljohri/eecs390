#ifndef MATLAB_H
#define MATLAB_H

#include "../matrix/matrix.h"

void matlab(Matrix**, Matrix**, Matrix**);
void matlab_ang(Matrix**);
void matlab_eul(Matrix**);
void matlab_rpy(Matrix**);

#endif