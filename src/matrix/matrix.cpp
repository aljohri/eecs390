#include <cstdlib>
#include <cstdio>
#include <cmath>

#include "../matrix/matrix.h"
#include "../exception/exception.h"
// A home-made matrix class we use to store the data for our rotation/transformation matrices, rather than use raw arrays. 
// Pretty straightforward overall, contains some extra functions we need and doesn't contain some standard ones we don't.

Matrix::Matrix() {//default constructor, just creates a 0x0 placeholder.
  table = NULL;
  rows = 0;
  cols = 0;	
}

Matrix::Matrix(const int row_count, const int column_count) {//Creats a row x column matrix, filled with zeroes.
  rows = row_count > 0 ? row_count : 0;
  cols = column_count > 0 ? column_count : 0; //Check to make sure we have valid input; otherwise, correct teh value to be 0.

  table = new double*[rows];//Assign memory for an array to store the locations of the array for each row
  for (int row = 0; row < rows; ++row) {//then for each row
  	table[row] = new double[cols];//assign memory for an array of doubles which represents the row.
  	for (int col = 0; col < cols; ++col) table[row][col] = 0;//and initialize to zero
  }
}

Matrix::~Matrix()
{
  // Deconstructor, cleans up allocated memory
  for (int r = 0; r < rows; r++) {//for each row
    delete table[r];//delete that row
  }
  delete table;//then delete the index of rows.
  table = NULL;
}

Matrix::Matrix(const Matrix& a) {//copy constructor
  rows = a.rows;//copy rows
  cols = a.cols;//copy columns
  table = new double*[a.rows];//index to store rows
  for (int r = 0; r < a.rows; r++) {//for each row
    table[r] = new double[a.cols];//assign the row's memory and store a pointer to it in storage table
      for (int c = 0; c < a.cols; c++) {
        table[r][c] = a.table[r][c];//then copy contents
      }
    }
}

int Matrix::GetRows() const { return rows; }//accessor for rows
int Matrix::GetCols() const { return cols; }//accessor for columns


void Matrix::set(const int row, const int col, const double val) {//mutator for the actual data in the matrix
  if (table != NULL && row > 0 && row <= rows && col > 0 && col <= cols) {//if the location is valid
    table[row-1][col-1] = val;//set the value in the  to the input
  }
  else {
    throw Exception("Subscript out of range");//If the input data refers to a location that doesn't exist, throw an exception.
  }
}


double Matrix::get(const int row, const int col) const{//accessor for the actual data in the matrix
  if (table != NULL && row > 0 && row <= rows && col > 0 && col <= cols) {//if the data is valid,
    return table[row-1][col-1];//retrieve the appropriate valye from the table
  }
  else {
    throw Exception("Subscript out of range");// but if the input data refers to a location that doesn't exist, throw an exception.
  }
}

double& Matrix::operator()(const int row, const int col) {//as above "get" mutator, except we return a pointer to the data. For use with operators.
  if (table != NULL && row > 0 && row <= rows && col > 0 && col <= cols) {
    return table[row-1][col-1];
  }
  else {
    throw Exception("Subscript out of range");// and if the input data refers to a location that doesn't exist, throw an exception.
  }
}

double& Matrix::operator()(const int row, const int col) const {//Again as above "get" and "operator", except now we hold data constant. For use with operators.
  if (table != NULL && row > 0 && row <= rows && col > 0 && col <= cols) {
    return table[row-1][col-1];
  }
  else {
    throw Exception("Subscript out of range");//and if the input data refers to a location that doesn't exist, throw an exception.
  }
}

Matrix& Matrix::operator= (const Matrix& a) {//assignment operator
  rows = a.rows;//copy rows 
  cols = a.cols;//copy columns
  table = new double*[a.rows];//assign new memory to store location of the rows
  for (int r = 0; r < a.rows; r++) {//and for each row,
    table[r] = new double[a.cols];//assign new memory to store the row
    for (int c = 0; c < a.cols; c++) {//then for each column
      table[r][c] = a.table[r][c];//copy
    }
  }
  return *this;
}

Matrix& Matrix::Add(const double v) {//add a constant
  for (int r = 0; r < rows; r++) {
    for (int c = 0; c < cols; c++) {
      table[r][c] += v;//add v to each row and column of the matrix.
    }
  }
   return *this;
}

Matrix& Matrix::Multiply(const double v) {//multiply by a constant
  for (int r = 0; r < rows; r++) {//for each row
    for (int c = 0; c < cols; c++)//for each column
      table[r][c] *= v;//multiply the contents of the cell by v
  }
   return *this;
}

Matrix& Matrix::Subtract(const double v) { return Add(-v); }//subtraction is negative addition
Matrix& Matrix::Divide(const double v) { return Multiply(1/v); }//division is inverse multiplication

/* Operator Overloading */

/* Unary Operator Overloads */

Matrix operator- (const Matrix& a) {//negation operator
  Matrix res(a.rows, a.cols);//create a result matrix

  for (int r = 0; r < a.rows; r++) {
    for (int c = 0; c < a.cols; c++) {
      res.table[r][c] = -a.table[r][c];//and set each value to the negative of what it originally was
    }
  }

  return res; //then return
}

/* (Matrix, Double) Binary Operator Overloads */

Matrix operator+ (const Matrix& a, const double b) { Matrix res = a; res.Add(b); return res; }//matrix-scalar addition operator, implemented above in "add"
Matrix operator+ (const double b, const Matrix& a) { Matrix res = a; res.Add(b); return res; }//ibid, reversed arguments
Matrix operator- (const Matrix& a, const double b) { Matrix res = a; res.Subtract(b); return res; }//matrix-scalar subtraction operator, implemented above in "subtract"
Matrix operator- (const double b, const Matrix& a) { Matrix res = -a; res.Add(b); return res; }//ibid, reversed arguments
Matrix operator* (const Matrix& a, const double b) { Matrix res = a; res.Multiply(b); return res; }//matrix-scalar multiplication operator, implemented above in "add"
Matrix operator* (const double b, const Matrix& a) { Matrix res = a; res.Multiply(b); return res; }//ibid, reversed arguments
Matrix operator/ (const Matrix& a, const double b) { Matrix res = a; res.Divide(b); return res; }//matrix-scalar division operator, implemented above in "add"
Matrix operator/ (const double b, const Matrix& a) { Matrix b_matrix(1, 1); b_matrix(1,1) = b; Matrix res = b_matrix / a; return res;}//ibid, reversed arguments

/* (Matrix, Matrix) Binary Operator Overloads */

Matrix operator+(const Matrix& a, const Matrix& b) {//matrix addition operator
  // check if the dimensions match
  if (a.rows == b.rows && a.cols == b.cols) {
    Matrix res(a.rows, a.cols);//we need to be the same size in both dimensions to add

    for (int r = 0; r < a.rows; r++) {//if so, for each row
      for (int c = 0; c < a.cols; c++) {//and each column
        res.table[r][c] = a.table[r][c] + b.table[r][c];//add the contents of the cells
      }
    }
    return res;
  }
  else {
    throw Exception("Dimensions do not match");//and if the dimensions don't match, throw an exception.
  }

  return Matrix();
}


Matrix operator- (const Matrix& a, const Matrix& b) {//matrix subtraction operator
  if (a.rows == b.rows && a.cols == b.cols) {
    Matrix res(a.rows, a.cols);//again, check size

    for (int r = 0; r < a.rows; r++) {//then for each cell
      for (int c = 0; c < a.cols; c++) {
        res.table[r][c] = a.table[r][c] - b.table[r][c];//subtract
      }
    }
    return res;
  }
  else {
    throw Exception("Dimensions does not match");//and if different size, throw an exception
  }

  return Matrix();
}

Matrix operator* (const Matrix& a, const Matrix& b) {//Matrix Multiplication operator
  if (a.cols == b.rows) {// check if the dimensions match. Height of one needs to be width of other
    Matrix res(a.rows, b.cols);//then create a space to store result
    for (int r = 0; r < a.rows; r++) {
      for (int c_res = 0; c_res < b.cols; c_res++) {
        for (int c = 0; c < a.cols; c++) {
          res.table[r][c_res] += a.table[r][c] * b.table[c][c_res];//and multiply the appropriate members.
        }
      }
    }
    return res;
  }
  else {
    throw Exception("Dimensions does not match");//if not right dimensions, throw exception
  }

  return Matrix();
}

Matrix operator/ (const Matrix& a, const Matrix& b) {//matrix division operator
  if (a.rows == a.cols && a.rows == a.cols && b.rows == b.cols) {//again, check dimensions
    Matrix res(a.rows, a.cols);//create output
    res = a * Inv(b);//and multiply by inverse of b
    return res;
  }
  else {
    throw Exception("Dimensions does not match");//different size, throw exception
  }

  return Matrix();
}



void Matrix::Print() const// print the contents of the matrix
{
  if (table != NULL)//If the matrix isn't empty
  {
    printf("[");
    for (int r = 0; r < rows; r++)//we will print the contents of each row formatted nicely
    {
      if (r > 0) printf(" ");
      for (int c = 0; c < cols-1; c++) printf("%.2f, ", table[r][c]);
      if (r < rows-1) printf("%.2f;\n", table[r][cols-1]);
      else printf("%.2f];\n", table[r][cols-1]);
    }
  }
  else
    printf("[ ];\n");//or if it's empty, just print square brackets
}


double Det(const Matrix& a)//returns the determinant of matrix a
{
  double d = 0;    // value of the determinant
  int rows = a.GetRows();
  int cols = a.GetRows();

  if (rows == cols) { // this is a square matrix
    if (rows == 1) { // this is a 1 x 1 matrix
      d = a.get(1, 1);
    }
    else if (rows == 2) { // this is a 2 x 2 matrix
      // the determinant of [a11,a12;a21,a22] is det = a11*a22-a21*a12
      d = a.get(1, 1) * a.get(2, 2) - a.get(2, 1) * a.get(1, 2);
    }
    else { // this is a matrix of 3 x 3 or larger
      for (int c = 1; c <= cols; c++) {
        Matrix M = a.Minor(1, c);
        //d += pow(-1, 1+c) * a(1, c) * Det(M);
        d += (c%2 + c%2 - 1) * a.get(1, c) * Det(M); // faster than with pow()
      }
    }
  }
  else {
    throw Exception("Matrix must be square");
  }
  return d;
}

// swap two values
void Swap(double& a, double& b) { double temp = a; a = b; b = temp; }

Matrix InvTrans(const Matrix& a){
  // calculates the inverse of a matrix in the special case of our transform robot matrices (3x3 orthogonal rotation with position vector/scaling factors.
	//Equations as per page 57 in niku; transpose, then use dot product for position column. 
	Matrix Res = Trans(a);
	Res(4,1)=0; Res(4,2)=0; Res(4,3)=0;
	Res(1,4)= -1*( a.get(1,4)*a.get(1,1) +a.get(2,4)*a.get(2,1)+a.get(3,4)*a.get(3,1));
	Res(2,4)= -1*( a.get(1,4)*a.get(1,2) +a.get(2,4)*a.get(2,2)+a.get(3,4)*a.get(3,2));
	Res(3,4)= -1*( a.get(1,4)*a.get(1,3) +a.get(2,4)*a.get(2,3)+a.get(3,4)*a.get(3,3));
	return Res;
}

/*
 * returns the inverse of Matrix a
 */
  // http://dyinglovegrape.wordpress.com/2010/11/30/the-inverse-of-an-orthogonal-matrix-is-its-transpose/

Matrix Inv(const Matrix& a) {
  Matrix res;
  double d = 0;    // value of the determinant
  int rows = a.GetRows();
  int cols = a.GetRows();

  d = Det(a);
  if (rows == cols && d != 0) { // this is a square matrix
    
    if (a * Trans(a) == Diag(rows)) { // this is an orthogonal matrix
      res = Trans(a);
      return res;
    }

    if (rows == 1) { // this is a 1 x 1 matrix
      res = Matrix(rows, cols);
      res(1, 1) = 1 / a.get(1, 1);
    }
    else if (rows == 2) { // this is a 2 x 2 matrix
      res = Matrix(rows, cols);

      res(1, 1) = a.get(2, 2);
      res(1, 2) = -a.get(1, 2);
      res(2, 1) = -a.get(2, 1);
      res(2, 2) = a.get(1, 1);

      res = (1/d) * res;
    }
    else if (rows == 3) {
      res = Matrix(rows, cols);

      res(1,1) = a(2,2) * a(3,3) - a(2,3) * a(3,2);
      res(1,2) = a(1,3) * a(3,2) - a(1,2) * a(3,3);
      res(1,3) = a(1,2) * a(2,3) - a(1,3) * a(2,2);
      
      res(2,1) = a(2,3) * a(3,1) - a(2,1) * a(3,3);
      res(2,2) = a(1,1) * a(3,3) - a(1,3) * a(3,1);
      res(3,3) = a(1,3) * a(2,1) - a(1,1) * a(2,3);

      res(3,1) = a(2,1) * a(3,2) - a(2,2) * a(3,1);
      res(3,2) = a(1,2) * a(3,1) - a(1,1) * a(3,2);
      res(3,3) = a(1,1) * a(2,2) - a(1,2) * a(2,1);

      res = (1/d) * res;
    }
    else if (rows == 4) {
    
    }
    else {
      throw Exception("to implement: use gauss-jordan elimination");
    }
  }
    else
    {
      if (rows == cols) {
        throw Exception("Matrix must be square");
      }
      else {
        throw Exception("Determinant of matrix is zero");
      }
    }
  return res;
}

int Matrix::Size(const int i) const {//Returns rows or columns, depending on value of i
  if (i == 1)
    return rows;
  else if (i == 2)
    return cols;
  return 0;
}


Matrix Trans(const Matrix& a) {//Calculate the transpose of a matrix
  int rows = a.GetCols();
  int cols = a.GetRows();
  Matrix res = Matrix(cols, rows);//create a new matrix, with number of rows and columns switched

  // then iterating through res matrix
  for (int r = 1; r <= cols; r++) {
    for (int c = 1; c <= rows; c++) {
      res(r,c) = a(c, r); //copy over data from the opposite location in the original
    }
  }

  return res;
}

/**
 * returns the minor from the given matrix where
 * the selected row and column are removed
 */
Matrix Matrix::Minor(const int row, const int col) const {
  Matrix res;
  if (row > 0 && row <= rows && col > 0 && col <= cols) {
    res = Matrix(rows - 1, cols - 1);

    // copy the content of the matrix to the minor, except the selected
    for (int r = 1; r <= (rows - (row >= rows)); r++) {
      for (int c = 1; c <= (cols - (col >= cols)); c++) {
        res(r - (r > row), c - (c > col)) = table[r-1][c-1];
      }
    }
  }
  else {
    throw Exception("Index for minor out of range");
  }

  return res;
}


bool operator== (const Matrix& a, const Matrix& b) {//equality operator
  if (a.rows != b.rows || a.cols != b.cols) return false; //if not same size, then not same

  for (int r = 0; r < a.rows; r++) {
    for (int c = 0; c < a.cols; c++) {
      if (a.table[r][c] != b.table[r][c]) return false;//if any location is different, then not same
    }
  }
  return true;//otherwise they are the same
}

Matrix Diag(const int n)//Create an n x n identity matrix
{
  Matrix res = Matrix(n, n); //create a matrix of zeroes
  for (int i = 1; i <= n; i++)
  {
    res(i, i) = 1;//then set each spot on the diagonal to 1m
  }
  return res;
}
