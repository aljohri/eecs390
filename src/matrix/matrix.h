#ifndef MATRIX_H
#define MATRIX_H

// A home-made matrix class we use to store the data for our rotation/transformation matrices, rather than use raw arrays. 
//Pretty straightforward overall, contains some extra functions we need and doesn't contain some standard ones we don't.
class Matrix;

//First, lets declare a few functions that take matrices as arguments or output, but are not elements of the Matrix class itself.
Matrix Diag(const int n);//returns an n x n identity matrix
Matrix Inv(const Matrix& a);//Calculate the inverse of the matrix.
Matrix InvTrans(const Matrix& a);//calculate inverse of transpose of matrix
Matrix Trans(const Matrix& a);//calculate the transpose of the matrix
Matrix Ones(const int rows, const int cols);//creates a matrix full of ones of the above size
Matrix Zeros(const int rows, const int cols);//ibid, except full of zeroes

double Det(const Matrix& a);//calculate determinant
   int Size(const Matrix& a, const int i);//gets rows or coulumns, depending on i.
  void Swap(double& a, double& b);//switches two numbers wherever they are in matrix

class Matrix {

//First, we overload basic operators to define how matrices interact with each other
friend bool operator== (const Matrix& a, const Matrix& b);//equality check operator
friend Matrix operator+ (const Matrix& a, const Matrix& b);//Standard matrix addition
friend Matrix operator+ (const Matrix& a, const double b);//Matrix-scalar addition
friend Matrix operator+ (const double b,  const Matrix& a);// Ibid, argument order reversed
friend Matrix operator- (const Matrix& a, const Matrix& b);//Standard matrix subtraction
friend Matrix operator- (const Matrix& a, const double b);//Matrix-scalar subtraction
friend Matrix operator- (const double b,  const Matrix& a);//Ibid, argument order reversed
friend Matrix operator- (const Matrix& a); //negation operator
friend Matrix operator* (const Matrix& a, const Matrix& b);//Standard matrix multiplication
friend Matrix operator* (const Matrix& a, const double b);//Matrix-scalar multiplication
friend Matrix operator* (const double b,  const Matrix& a);//Ibid., argument order reversed
friend Matrix operator/ (const Matrix& a, const Matrix& b);//Standard matrix division
friend Matrix operator/ (const Matrix& a, const double b);//Matrix-scalar division
friend Matrix operator/ (const double b,  const Matrix& a);//Ibid., argument order reversed

public:
  Matrix();//Standard constructor, creates 0 x 0 placeholder 
  Matrix(const int row_count, const int column_count); //Creats a row x column matrix, filled with zeroes.
  Matrix(const Matrix& a);//Copy constructor. Creates a matrix that's a copy of a preexisting matrix.
  ~Matrix();//deconstructor

  double& operator()(const int r, const int c);//like the above "get", but we return a pointer to the data instead of data itself. Used in operator functions.
  double& operator()(const int row, const int col) const;

  void set(const int r, const int c, const double val);//Mutator for the contents of the actual matrix at row r column c

  Matrix& operator= (const Matrix& a); //assignment operator
  Matrix& Add(const double v); //Returns a new matrix which is this matrix plus a constant
  Matrix& Subtract(const double v);//Returns a new matrix which is this matrix minus a constant
  Matrix& Multiply(const double v);//Returns a new matrix which is this matrix times a constant
  Matrix& Divide(const double v); //Returns a new matrix which is this matrix divided by a constant
  Matrix Minor(const int row, const int col) const; //Returns the determinant of a 3*3 subsection of our matrix.

  int Size(const int i) const;//returns rows or columns, based on i; 1=rows
  double get(const int r, const int c) const; //Accessor for contents of the actual matrix
  int GetRows() const;//Accessor for rows
  int GetCols() const;//Accessor for columns
  void Print() const;//Prints the matrix.

private://the acutal contents of the matrix. It stores:
  int rows;//the number of rows
  int cols;//the number of columns
  double** table; //and a rows x columns array of arrays with the actual data in it.
};

#endif

