#include <cmath>
#include "../matrix/matrix.h"

// Compute all three reverse methods of reverse transforms (one-axis, euler angles, and RPY angles)
void reverseTransform(Matrix A, double* angleaxis, double* euler, double* rpy) {
	double theta, xx, yy, zz;
	double eul1, eul2, eul3, sp, cp;
	double r, y, p, sy, cy;

	// Compute Axis-Angle or One-Axis Reverse Transformation
	theta = std::acos(0.5 * (A(1,1) + A(2,2) + A(3,3) - 1));
	xx = (A(3,2) - A(2,3)) / (2 * std::sin(theta));
	yy = (A(1,3) - A(3,1)) / (2 * std::sin(theta));
	zz = (A(2,1) - A(1,2)) / (2 * std::sin(theta));

	// Compute Euler Angle Reverse Transformation
	// eul1 = phi, eul2 = theta, eul3 = psi
	eul1 = std::atan2(A(2,3), A(1,3));
	sp = std::sin(eul1);
	cp = std::cos(eul1);
	eul2 = std::atan2((cp * A(1,3)) + (sp * A(2,3)), A(3,3));
	eul3 = std::atan2((-sp * A(1,1)) + (cp * A(2,1)), (-sp * A(1,2)) + (cp * A(2,2)));

	// Compute RPY Reverse Transformation
	// print reverse (y p r)
	r = atan2(A(3,2), A(3,3));
	y = atan2(A(2,1), A(1,1));
	sy = std::sin(y);
	cy = std::cos(y);
	p = atan2(-A(3,1), (cy * A(1,1)) + (sy * A(2,1)));

	// Take local variables and set them to the array arguments so they can be used outside this function
	angleaxis[0] = theta; angleaxis[1] = xx; angleaxis[2] = yy; angleaxis[3] = zz;
	euler[0] = eul1; euler[1] = eul2; euler[2] = eul3;
	rpy[0] = r; rpy[1] = p; rpy[2] = y;
}
