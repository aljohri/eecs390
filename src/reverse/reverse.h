#ifndef REVERSE_H
#define REVERSE_H

#include "../matrix/matrix.h"

// Compute all three reverse methods of reverse transforms (one-axis, euler angles, and RPY angles)
void reverseTransform(Matrix A, double* angleaxis, double* euler, double* rpy);

#endif