#ifndef ROTATIONS_H
#define ROTATIONS_H

#include <cmath>

#include "../matrix/matrix.h"

// Create Rotation Matrix from "angle" in radians along the x axis
Matrix rotx(float angle) {
    Matrix A = Matrix(4,4); float c = cos(angle); float s = sin(angle);
    A(1,1) = 1;  A(1,2) = 0;  A(1,3) = 0;  A(1,4) = 0;
    A(2,1) = 0;  A(2,2) = c;  A(2,3) = -s; A(2,4) = 0;
    A(3,1) = 0;  A(3,2) = s;  A(3,3) = c;  A(3,4) = 0;
    A(4,1) = 0;  A(4,2) = 0;  A(4,3) = 0;  A(4,4) = 1;
    return A;
}

// Create Rotation Matrix from "angle" in radians along the y axis
Matrix roty(float angle) {
    Matrix A = Matrix(4,4); float c = cos(angle); float s = sin(angle);
    A(1,1) = c;  A(1,2) = 0;  A(1,3) = s;  A(1,4) = 0;
    A(2,1) = 0;  A(2,2) = 1;  A(2,3) = 0;  A(2,4) = 0;
    A(3,1) = -s; A(3,2) = 0;  A(3,3) = c;  A(3,4) = 0;
    A(4,1) = 0;  A(4,2) = 0;  A(4,3) = 0;  A(4,4) = 1;
    return A;
}

// Create Rotation Matrix from "angle" in radians along the z axis
Matrix rotz(float angle) {
    Matrix A = Matrix(4,4); float c = cos(angle); float s = sin(angle);
    A(1,1) = c;  A(1,2) = -s; A(1,3) = 0;  A(1,4) = 0;
    A(2,1) = s;  A(2,2) = c;  A(2,3) = 0;  A(2,4) = 0;
    A(3,1) = 0;  A(3,2) = 0;  A(3,3) = 1;  A(3,4) = 0;
    A(4,1) = 0;  A(4,2) = 0;  A(4,3) = 0;  A(4,4) = 1;
    return A;
}

#endif