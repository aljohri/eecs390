#ifndef ROTATIONS_H
#define ROTATIONS_H

#include "../matrix/matrix.h"

Matrix rotx(float angle); // Create Rotation Matrix from "angle" in radians along the x axis
Matrix roty(float angle); // Create Rotation Matrix from "angle" in radians along the y axis
Matrix rotz(float angle); // Create Rotation Matrix from "angle" in radians along the z axis

#endif