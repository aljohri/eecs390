
#include "../utilities/utilities.h"
#include "../matrix/matrix.h"
#include "../exception/exception.h"

// Copy Position from P1,P2,P3, and/or P4 
// into 4 Element Column vector  or 4x4 Matrix A
void setPosition(Matrix &A, int P1, int P2, int P3, int P4 = 1) {
	if (A.Size(2) == 1) { // 4 element column vector
		A(1,1) = P1; A(2,1) = P2; A(3,1) = P3; A(4,1) = P4;
	}
	else if (A.Size(2) == 4) { // 4x4 square matrix
		A(1,4) = P1; A(2,4) = P2; A(3,4) = P3; A(4,4) = P4;
	}
	else throw Exception("Only use this for 4 element column vector or 4x4 square matrix.");
}

// Copy Position from 4x4 Matrix (B) to Column Vector (A) 
// or Copy Position from 4x4 Matrix (B) to 4x4 Matrix (A)
void setPosition(Matrix &A, Matrix &B) {
	if (A.Size(1) != B.Size(1) || B.Size(2) != 4) throw Exception("Dimensions do not match");
	if (A.Size(2) == 1) {
		A(1,1) = B(1,4);  A(2,1) = B(2,4);  A(3,1) = B(3,4);  A(4,1) = B(4,4);
	}
	else if (A.Size(2) == 4) {
		A(1,4) = B(1,4);  A(2,4) = B(2,4);  A(3,4) = B(3,4);  A(4,4) = B(4,4);		
	}
	else {
		throw Exception("Dimensions do no match");
	}

}